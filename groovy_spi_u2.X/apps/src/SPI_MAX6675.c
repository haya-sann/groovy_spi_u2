#include <xc.h>
#include "supplement.h"
#include "SPI_MAX6675.h"

void InitSPI_MAX6675(){
    SSP1STAT = 0b00000000;  //SMP: middle/ CKE: IdleToActive
    SSP1CON1 = 0b00100010;  //SSPEN/ CKP: Idle Low/ SPIMaster Fosc/16
}

int Get_MAX6675(){
    int msb, lsb, msblsb;

    // MSB
    SSPBUF = 0xFF;
    while(!SSPSTATbits.BF);
    msb = SSPBUF;
    
    // LSB
    SSPBUF = 0xFF;
    while(!SSPSTATbits.BF);
    lsb = SSPBUF;

    msblsb = msb << 8;
    msblsb = msblsb + lsb;
    
    return msblsb;
}

int Get_MAX6675_Temp(int aMAX6675){
    int temp, temparature;
    
    temp = aMAX6675 >> 3;
    temparature = temp / 4;

    return temparature;
}

int Check_MAX6675_NC(int aMAX6675){
    unsigned char ConnectionOrNot;
    ConnectionOrNot = bittst(aMAX6675, 0b0000000000000100);
        
    return ConnectionOrNot;
}

