/* 
 * File:   IO.h
 * Author: ooo
 *
 * Created on February 2, 2022, 6:30 PM
 */

#ifndef IO_H
#define	IO_H

#ifdef	__cplusplus
extern "C" {
#endif

    extern void InitIO();

#ifdef	__cplusplus
}
#endif

#endif	/* IO_H */

