/********************************************************************
 Software License Agreement:

 The software supplied herewith by Microchip Technology Incorporated
 (the "Company") for its PIC(R) Microcontroller is intended and
 supplied to you, the Company's customer, for use solely and
 exclusively on Microchip PIC Microcontroller products. The
 software is owned by the Company and/or its supplier, and is
 protected under applicable copyright laws. All rights are reserved.
 Any use in violation of the foregoing restrictions may subject the
 user to criminal sanctions under applicable laws, as well as to
 civil liability for the breach of the terms and conditions of this
 license.

 THIS SOFTWARE IS PROVIDED IN AN "AS IS" CONDITION. NO WARRANTIES,
 WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *******************************************************************/

/** INCLUDES *******************************************************/
#include <system.h>

#include <stdint.h>
#include <string.h>
#include <stddef.h>

#include <usb/usb.h>

#include <app_led_usb_status.h>
#include <app_device_cdc_basic.h>
#include <usb_config.h>

#include "SPI_MAX6675.h"
#include "supplement.h"

/** VARIABLES ******************************************************/

static uint8_t readBuffer[CDC_DATA_OUT_EP_SIZE];
static uint8_t writeBuffer[CDC_DATA_IN_EP_SIZE];

/*********************************************************************
* Function: void APP_DeviceCDCEmulatorInitialize(void);
*
* Overview: Initializes the demo code
*
* PreCondition: None
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceCDCEmulatorInitialize()
{
    CDCInitEP();
    
    line_coding.bCharFormat = 0;
    line_coding.bDataBits = 8;
    line_coding.bParityType = 0;
    line_coding.dwDTERate = 9600;
    
}

/*********************************************************************
* Function: void APP_DeviceCDCEmulatorTasks(void);
*
* Overview: Keeps the demo running.
*
* PreCondition: The demo should have been initialized and started via
*   the APP_DeviceCDCEmulatorInitialize() and APP_DeviceCDCEmulatorStart() demos
*   respectively.
*
* Input: None
*
* Output: None
*
********************************************************************/
void APP_DeviceCDCEmulatorTasks()
{
    /* If the USB device isn't configured yet, we can't really do anything
     * else since we don't have a host to talk to.  So jump back to the
     * top of the while loop. */
    if( USBGetDeviceState() < CONFIGURED_STATE )
    {
        return;
    }

    /* If we are currently suspended, then we need to see if we need to
     * issue a remote wakeup.  In either case, we shouldn't process any
     * keyboard commands since we aren't currently communicating to the host
     * thus just continue back to the start of the while loop. */
    if( USBIsDeviceSuspended()== true )
    {
        return;
    }
        

    /* Check to see if there is a transmission in progress, if there isn't, then
     * we can see about performing an echo response to data received.
     */
    if( USBUSARTIsTxTrfReady() == true)
    {
        uint8_t i, j;
        uint8_t numBytesRead;
        
        char max_6675_temp_str[8], max_6675_temp_str_len;
        int max_6675, max_6675_temp;

        numBytesRead = getsUSBUSART(readBuffer, sizeof(readBuffer));

        /* For every byte that was read... */
        for(i=0; i<numBytesRead; i++)
        {
            for(j = 0; j < 8;j++){max_6675_temp_str[j] = 0;}
            switch(readBuffer[i])
            {
                case 'A':
                    LATCbits.LATC2 = 1;
                    break;
                case 'a':
                    LATCbits.LATC2 = 0;
                    break;

                case 'B':
                    LATCbits.LATC3 = 1;
                    break;
                case 'b':
                    LATCbits.LATC3 = 0;
                    break;
  
                case '0':
                    LATCbits.LATC4 = 0;
                    max_6675 = Get_MAX6675();
                    LATCbits.LATC4 = 1;
                
                    if(Check_MAX6675_NC(max_6675)){
                        putsUSBUSART("NC\r\n");
                    }
                    else{
                        max_6675_temp = Get_MAX6675_Temp(max_6675);
                        my_itoa(max_6675_temp, max_6675_temp_str, 10);
                        max_6675_temp_str_len = my_strlen(max_6675_temp_str);
                        max_6675_temp_str[max_6675_temp_str_len] = '\r';
                        max_6675_temp_str[max_6675_temp_str_len + 1] = '\n';
                        putsUSBUSART(max_6675_temp_str);
                    }
                    break;

                case '1':
                    LATCbits.LATC5 = 0;
                    max_6675 = Get_MAX6675();
                    LATCbits.LATC5 = 1;

                    if(Check_MAX6675_NC(max_6675)){
                        putsUSBUSART("NC\r\n");
                    }
                    else{
                        max_6675_temp = Get_MAX6675_Temp(max_6675);
                        my_itoa(max_6675_temp, max_6675_temp_str, 10);
                        max_6675_temp_str_len = my_strlen(max_6675_temp_str);
                        max_6675_temp_str[max_6675_temp_str_len] = '\r';
                        max_6675_temp_str[max_6675_temp_str_len + 1] = '\n';
                        putsUSBUSART(max_6675_temp_str);
                    }
                    break;

            /*
                // If we receive new line or line feed commands, just echo
                // them direct.
                
                case 0x0A:
                case 0x0D:
                    writeBuffer[i] = readBuffer[i];
                    break;

                // If we receive something else, then echo it plus one
                // so that if we receive 'a', we echo 'b' so that the
                // user knows that it isn't the echo enabled on their
                // terminal program.
                
            */
                default:
//                    writeBuffer[i] = readBuffer[i];

                    break;
            }
        }

        if(numBytesRead > 0)
        {
            // After processing all of the received data, we need to send out
            // the "echo" data now.
            
            putUSBUSART(writeBuffer,numBytesRead);
        }

    }
    CDCTxService();
}