/**
 * @file      shell.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-SPI (Version 1.0.0)
 * Copyright (c) 2022 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2022 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include "USART.h"
#include "SPI_MAX6675.h"
#include "supplement.h"

void shell()
{
    char max_6675_temp_str[8], max_6675_temp_str_len;
    int max_6675, max_6675_temp;

    switch (gRxChar) {
        case 'A':
            LATCbits.LATC2 = 1;
            break;
        case 'a':
            LATCbits.LATC2 = 0;
            break;

        case 'B':
            LATCbits.LATC3 = 1;
            break;
        case 'b':
            LATCbits.LATC3 = 0;
            break;

        case '0':
            gRxChar = 0;
            LATCbits.LATC4 = 0;
            max_6675 = Get_MAX6675();
            LATCbits.LATC4 = 1;
        
            if(Check_MAX6675_NC(max_6675)){
                strOutFromUSART("NC\r\n");
            }
            else{
                max_6675_temp = Get_MAX6675_Temp(max_6675);
                my_itoa(max_6675_temp, max_6675_temp_str, 10);
                strOutFromUSART(max_6675_temp_str);
                strOutFromUSART("\r\n");
            }
            break;

        case '1':
            gRxChar = 0;
            LATCbits.LATC5 = 0;
            max_6675 = Get_MAX6675();
            LATCbits.LATC5 = 1;

            if(Check_MAX6675_NC(max_6675)){
                strOutFromUSART("NC\r\n");
            }
            else{
                max_6675_temp = Get_MAX6675_Temp(max_6675);
                my_itoa(max_6675_temp, max_6675_temp_str, 10);
                strOutFromUSART(max_6675_temp_str);
                strOutFromUSART("\r\n");
            }
            break;

        default:
            LATCbits.LATC4 = 1;
            LATCbits.LATC5 = 1;
            break;           
    }
}
