/* 
 * File:   shell.h
 * Author: ooo
 *
 * Created on February 15, 2022, 8:30 AM
 */

#ifndef SHELL_H
#define	SHELL_H

#ifdef	__cplusplus
extern "C" {
#endif

    extern void shell();


#ifdef	__cplusplus
}
#endif

#endif	/* SHELL_H */

