
/**
 * @file      USART.c
 * @author    Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * Groovy-SPI (Version 1.0.0)
 * Copyright (c) 2022 Osamu OHASHI (Omiya-Giken LLC)
 * ===============================================================
 * The MIT License : https://opensource.org/licenses/MIT
 *
 * Copyright (c) 2022 Osamu OHASHI (Omiya-Giken LLC)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <xc.h>
#include "USART.h"

void InitUSART()
{
    RCSTA   = 0b10010000;   // UART送受信を有効
    TXSTA   = 0b00100100;   // 8Bit非同期送受信
    
    OSCCON = 0b11110100; // Refer PIC16F1559 manual P.73   
    BAUDCON = 0b00001000;   // HI-16Bitボーレート
    SPBRG  = 103;           // 9600
}

void putch(unsigned char byte)
{
    while(!TXIF);
    TXREG = byte;
}

unsigned char getch()
{
    while(!RCIF);
    return RCREG;
}

unsigned char getche()
{
    unsigned char c;
    c = getch();
    putch(c);
    return c;
}

void strOutFromUSART(const char *str)
{
    while(*str){                     //文字列の終わり(00)まで継続
        putch(*str++);
    }
}
