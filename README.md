# groovy_spi_u2

Integrate UART & USB capabilties in one firmware.

## Introduction
|Groovy-SPI|Groovy-SPI with GPIO-wire and MAX6675 sensor|Modified Toaster-Oven|
|-|-|-|
|![IMG_1793](/uploads/b3c99edfa739d55a8e48ca380de41808/IMG_1793.jpeg)|![IMG_1800](/uploads/983664316e263146fd5e63a7dbf938da/IMG_1800.jpeg)|![IMG_1682](/uploads/76757ee1cfa760caaa20f0b2bb7ae736/IMG_1682.jpeg)|

 Groovy-SPI is an USB device which has SPI, GPIO, UART functionalities. Will be released soon from [Omiya-Giken LLC](http://www.omiya-giken.com/?lang=en). groovy_spi_u2 is firmware, it is designed for heater control application such as simple toaster oven. Two MAX6675 K-thermo couple SPI and SSR control via GPIO-Output are equiped for the application. You need tiny cutomized toaster oven with K-thermo couple MAX6675.

## Microchip MPLAB project
 This FW is developed under Microchip genuine development tool as MPLAB-X. You can clone then start develop quickly.

## Dual mode firmware
 Groovy-SPI_U2 integrates both USB and UART firmware in one hex-file. You can choose at boot-up with jumper of mid position of J1.
 
 |UART mode|Jumper setting@boot|
 |-|-|
 |![IMG_1797](/uploads/181676ff1605632da72a3041824a605f/IMG_1797.jpeg)|![IMG_1794](/uploads/0c32e63c63268ea738725e545d25ddfa/IMG_1794.jpeg)|
 

  * USB mode:  
 Not-jumpered will introduce USB mode as CDC-ACM. CDC-ACM is very matured USB communication class. Maybe, you don't need to install driver softwares.  

  * UART mode:  
 Jumpered will introduce simple UART-mode. You can connect your own MCU UART asset such as Groovy-IoT and others. 

## How to use
  Groovy-SPI is looks like modem-device from host side (such as PC, Mac and Raspberry-Pi, etc). Simply start terminal software (e.g. Teraterm, Screen commnand of UNIX system) with communication parameters 9600-8N1, then you can press a commnad letter for invoking each function.

### Command letters
 Groovy-SPI handles two K thermo couple heat sensors and two digital output for SSR. Simply type ```0``` or ```1```, you can get Celsius temparature. Type ```A``` (capitalized) or ```a``` and ```B``` or ```b``` each channel output ```H``` or ```L``` level signal, you can confirm which level by LED status.  

### Thermo couple sensor
 MAX6675 SPI sensors are sold in anywhere (e.g [Aliexpress](https://www.aliexpress.com/item/32985892905.html?spm=a2g0o.productlist.0.0.8e3933a3hQKBLV&algo_pvid=5149dff3-3888-4f9c-8fc3-9a2164aacff9&algo_exp_id=5149dff3-3888-4f9c-8fc3-9a2164aacff9-6&pdp_ext_f=%7B%22sku_id%22%3A%2266895340285%22%7D&pdp_pi=-1%3B359.0%3B-1%3B-1%40salePrice%3BJPY%3Bsearch-mainSearch)) and very resonable prices. You have to make connector cable by yourself. Pin out refer below table.

### Solid State Relay (SSR)
 [SSR](https://www.aliexpress.com/wholesale?catId=0&initiative_id=AS_20220214234124&isPremium=y&SearchText=solid+state+relay) can be controlled digital output from Groovy-SPI digital out terminal.

 * Dumper registers (R2, R4)  
 You can see 330Ω Registers both R2 and R4. You have to replace those regs with adjusting drive-current of SSR, if you need.

### Pin out
  Connectors and jumper-pins have symbol mark (e.g. ```△``` or ```「```). Those marks indicate the 1st-pin (start pin). 

#### J1: Input
|Pin: Usage|Pin: Usage|
|-|-|
|```1```: GND|```4```: MCLR|
|```2```: GND|```5```: RA4- **Mode switch**|
|```3```: GND|```6```: RA5|
  
#### J3: ICSP
|Pin|Usage@Ver.1.0.0|
|-|-|
|```1```|VPP|
|```2```|Power|
|```3```|GND|
|```4```|**ICSPCLK**|
|```5```|**ICSPDAT**|
|```6```|NC|

***Note: Ver1.0.0 flipped ICSPCLK/ISCPDTA from usual, Be careful re-programming.***

#### J4: SPI x 2ch
|Pin|Pin|Usage|
|-|-|-|
|```1```|```2```|Power|
|```3```|```4```|GND|
|```5```|```6```|SDI@**PIC**|
|```7```|```8```|SDO@**PIC**|
|```9```|```10```|CSx |
|```11```|```12```|SCK@**PIC**|

***Note: ```「``` indicates 1-pin.***

#### J7: [UART](https://www.digikey.com/en/products/detail/jst-sales-america-inc/XHP-4/683353)
|Pin|Usage|
|-|-|
|```1```|Power|
|```2```|GND|
|```3```|TX@**PIC**|
|```4```|RX@**PIC**|

***Note: I'm not sure, recently this type of peripheral (e.g. TX/RX) pins table indicate against...When start that style ?? I'll describe still PIC(Device, not host) side !!***

#### J5, J6: [Ditigal out](https://www.digikey.com/en/products/detail/jst-sales-america-inc/XHP-2/555485)
|Pin|Usage|
|-|-|
|```1```|Power|
|```2```|GND|

